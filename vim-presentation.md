%title: My Vim Ted Talk
%author: Daniel Celebucki

-> # My Vim Ted Talk <-

-> # By: Daniel Celebucki <-


---------------

-> # Table of Contents <-

- Why Vim / History
- Basics
- Learning Curve / Phases of Learning Vim
- What Do You Really Need
- Using Vim
- vimrc
- Plugins
- Other cool things
- Vi vs Vim

---------------


-> # Why Vim / History <-

- Ubiquitous
- vi exists on all Unix systems and most linux ones
- Customizable
    - vimrc
    - gvimrc
    - plugins
- Efficiency
- Edit text at the speed of thought
- Old Lear machine
- History chart

---------------

-> # Basics <-

- Modal editor
    - Command Mode
        - Default mode when you start vim. 
        - Each key has an associated command
        - `<esc>` to return to command mode
    - Insert Mode
        - This is the mode where you'll do you typing
        - `i a I A` most commonly used to enter insert mode
    - Visual Mode
        - Mode that allows you to select text and then apply an action to it
        - `v V and <C-v>` allow you to enter visual mode
        - I personally don't think you should rely on this mode

---------------

-> # Basics <-

- Vim as a language
    - Count Operator Motion
    - Count -- any number
    - Operator
        - `c -- change`
        - `d -- delete`
        - `y -- yank (copy)`
    - Motion (Text is intrinsically structured)
        - `w -- words`
        - `j k -- lines`
        - `"" -- quotes`
        - `( ) -- sentences/parenthesis`
        - `{ } -- blocks`
        - etc

- Examples
    - `cw -- change word`
    - `ciw -- change inner word`
    - `caw -- change outer word`
    - `ci" -- change inner quotes`
    - `2cw -- change 2 words`
    - `dj -- delete 1 line down`
    - `d5j -- delete 5 lines down`

---------------

-> # Learning Curve / Phases of Learning Vim <-

- *Use vimtutor*

-  How do I get out of this program!!!
-  I can use vim to edit a basic file 
    - `i <esc> h j k l :wq`
-  I can use vim to edit a file, I'm still slower than regular text editors 
    - `g G #G w b e $ 0 x dd`
-  I can use vim to edit a file, I'm as fast as a regular text editor
    - `o O A I u <ctrl-r> <ctrl-u> <ctrl-d> / n N W B C S :vsp :sp :e .`
-  I can use vim to edit a file, I'm faster than a regular text editor
    - `<ctrl-o> <ctrl-i> f t ( ) { } ; , <ctrl-t> <ctrl-d> :ls :b# :bd`

---------------

-> # How do I get out of this program!!! <-

- Accidentally launched vim
- git commit
- visudo

---------------

-> # I can use vim to edit a basic file <-

- `i <esc> h j k l :wq`
- `i` -- insert mode
- `<esc>` -- get to command mode
- `h` -- go left one character
- `j` -- go down one line
- `k` -- go up one line
- `l` -- go right one character
- `:w` -- save file
- `:q` -- quit file
- `:wq` -- save and quit

---------------

-> # I can use vim to edit a file, I'm still slower than regular text editors <-

- `gg G #G w b e $ 0 x dd`
- `gg` -- go to the very first line
- `G` -- go to the last line
- `#G` -- go to line #
- `w` -- move one word forward (augment with f)
- `b` -- move one word back (augment with f)
- `e` -- move to the end of the next word (augment with f)
- `$` -- move to the end of the line
- `0` -- move to the beginning of the line
- `x` -- delete a character
- `dd` -- delete a line

---------------

-> # I can use vim to edit a file, I'm as fast as a regular text editor <-

- `o O A I <ctrl-u> <ctrl-d> u <ctrl-r> / n N W B C S :vsp :sp :e .`
- `o` -- insert line below current line and go into insert mode
- `O` -- insert line above current line and go into insert mode
- `A` -- enter insert mode at the end of the line
- `I` -- enter insert mode at the beginning of the line
- `<ctrl-u>` -- Half page up (Money makers)
- `<ctrl-d>` -- Half page up (Money makers)
- `u` -- undo
- `<ctrl-r>` -- redo
- `/` -- Search for something (Money makers) 
- `n` -- go to next location that matched the search
- `N` -- go to the previous location that matched the search
- `W` -- move forward one WORD
- `B` -- move backward one WORD
- `C` -- change the rest of the line from where you're at
- `S` -- Delete a whole line and enter insert mode
- `:vsp` -- open a vertical split
- `:sp` -- open a horizontal split
- `:e` -- open a file for editing
- `.` -- repeat the last action

---------------

-> # I can use vim to edit a file, I'm faster than a regular text editor <-

- `<ctrl-o> <ctrl-i> f t ( ) { } ; , <ctrl-t> <ctrl-d> :ls :b# :bd`
- `<ctrl-o>` -- go back to your last position you were at
- `<ctrl-i>` -- go forward to your next position you were at
- `f<char>` -- go to the next matching character on the line you're on (Money maker)
- `t<char>` -- go one character before the next matching character on the line you're on
- `( )` -- go to the next/previous sentence. Or parenthesis for change
- `{ }` -- go to the next/previous block. Or {} for change/delete
- `;` -- go to the next character that matches f or t (Money maker)
- `,` -- go to the previous character that matches f or t (Money maker)
- `<ctrl-t>` -- indent forwards a line while in insert mode
- `<ctrl-d>` -- indent backwards a line while in insert mode
- `:ls` -- show other buffers
- `:b#` -- go to that buffer
- `:bd` -- delete a buffer

---------------

-> # What do you really need <-

- Vertical Movement
    - gg
    - G
    - <ctrl-u>
    - <ctrl-d>
    - j
    - k
    - <#>G
    - / 
- Horizontal Movement
    - f<char>
    - ;
    - ,
    - w/W
    - b/B
- Inserting/Deleting/Changing text
    - i I a A o O
    - caw / ciw
    - ci" / ca"
    - ci( / ca(
    - C
    - S
    - various other change motions

---------------

-> # Using Vim <-

- Demonstration time
- Follow along file -- step-by-step.txt

---------------

-> # vimrc <-

- set
- help menu
- shift-k

---------------

-> # Plugins <-

- How to install a plugin
- Plugin manager
- YouCompleteMe

---------------

-> # Other cool things <-

- :Ex :Sex :Vex
- Windows vs Tabs
- zz zt zb
- \>> << >55G >}
- :%s//something/g
- Macros
- Autocomplete
- :set relativenumber
- <C-v>I and <C-v>A
- Spell checker
- Editing a read-only file
- checking if a swap file has anything new

---------------

-> # Vi vs Vim <-

- :help vi_diff

---------------

- Resources
    - vimtutor
    - You don't grok vim -- https://gist.github.com/nifl/1178878 
    - Vimrc options and what they do -- https://dougblack.io/words/a-good-vimrc.html
    - From Vim Muggle to Wizard in 10 Easy Steps -- https://www.youtube.com/watch?v=MquaityA1SM


