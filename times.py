#!/usr/bin/python3
import sys
import datetime

start_time = datetime.datetime.strptime(sys.argv[1], "%H%M")

print('{:%H%M} -- Start Levain'.format(start_time))
print('{:%H%M} -- Autolyse Dough'.format(start_time + datetime.timedelta(hours=3, minutes=30)))
print('{:%H%M} -- Mix with Levain'.format(start_time + datetime.timedelta(hours=5, minutes=0)))
print('{:%H%M} -- Mix with Salt'.format(start_time + datetime.timedelta(hours=5, minutes=30)))
print('{:%H%M} -- Bulk Ferment'.format(start_time + datetime.timedelta(hours=5, minutes=35)))
print('{:%H%M} -- Fold#1'.format(start_time + datetime.timedelta(hours=5, minutes=50)))
print('{:%H%M} -- Fold#2'.format(start_time + datetime.timedelta(hours=6, minutes=5)))
print('{:%H%M} -- Fold#3'.format(start_time + datetime.timedelta(hours=6, minutes=20)))
print('{:%H%M} -- Fold#4'.format(start_time + datetime.timedelta(hours=6, minutes=50)))
print('{:%H%M} -- Fold#5'.format(start_time + datetime.timedelta(hours=7, minutes=20)))
print('{:%H%M} -- Fold#6'.format(start_time + datetime.timedelta(hours=7, minutes=50)))
print('{:%H%M} -- split and preshape dough'.format(start_time + datetime.timedelta(hours=10, minutes=0)))
print('{:%H%M} -- Cold proof for 12-14 hours'.format(start_time + datetime.timedelta(hours=10, minutes=20)))
